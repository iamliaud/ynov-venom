# Environnement Docker pour développeur Javascript
*écrit par Jean-Louis Liaud*

<br/><br/>
<p align="center">
![N|Solid](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQB8fUZk0raTcKuyp4TGRKhgZQPHrx_Qzk-JqCUenmLHLbVk4sw)
</p>

> Je travaille essentiellement sur des technologies JavaScript. Le but de ce projet est de monter un environnement sous docker qui permet l'utilisation simple et rapide des technologies que j'utilise dans mes projets.

<br/><br/>

### :computer: Technologies

Dans ce répository, nous avons Dockerisé plusieurs technologies : 

* [VueJS](https://vuejs.org/) - Framework JavaScript open-source utilisé pour construire des interfaces utilisateur.
* [NodeJS](https://nodejs.org/fr/) - Technologie permettant d'exécuter du JavaScript côté serveur.
* [ExpressJS](https://expressjs.com/fr/) - Framework permettant de construire des applications web basées sur NodeJS.
* [MongoDB](https://www.mongodb.com/fr) - Système de gestion de base de données orienté documents ne nécessitant pas de schéma prédéfini des données.
* [Mongo-Express](https://github.com/mongo-express/mongo-express) - Interface d'administration basée sur MongoDB et écrite avec Node.js, Express et Bootstrap3.

<br/>

### :runner: Etapes pour démarrer

1. Cloner le répository
    ```sh
    https://gitlab.com/iamliaud/ynov-venom.git
    ```

2. Naviger dans le dossier ynov-venom
     ```sh
    cd ynov-venom
    ```

3. Monter vos containers
     ```sh
    docker-compose up
    ```

Vos applications devraient tourner sur :
* [NodeJS/ExpressJS] - http://localhost:8091
* [VueJS] - http://localhost:8090
* [Mongo-Express] - http://localhost:8082
* [Mongo] - http://localhost:27017

<br/>

### :warning: Warnings

* S'assurer que les ports suivant ne sont pas occupés : 8082, 8090, 8091, 27017
* Bien attendre que les containers soient construit avant de vouloir accéder à la solution.

<br/>

### :x: Erreurs

Une erreur indiquant de changer les identifiants de la base de données.

  ```sh
  Server is open to allow connections from anyone (0.0.0.0)
  mongo-express_1  | basicAuth credentials are "admin:pass", it is recommended you change this in your config.js!
  ```

<br/>

### :pencil2: Contact

| Plateformes | Liens |
| ------ | ------ |
| LinkedIn | https://www.linkedin.com/in/jean-louisliaud/ |
| Website | www.iamliaud.fr |

<br/>

> **Pray to god Mongo, or Mongod to pray :pray:**